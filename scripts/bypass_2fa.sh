#!/bin/bash

# Log into control panel to establish two factor auth for ssh/sftp purposes.

! which curl >/dev/null && printf "Please install curl.\n" && exit 1
! which jq >/dev/null && printf "Please install jq.\n" && exit 1

url=https://members.mayfirst.org/cp/api.php
user="$1"
password="$2"
if [ -z "$user" -o -z "$password" ]; then
	echo ""
	echo "Usage: $0 SSH_USERNAME SSH_PASSWORD"
	echo ""
	exit 1
fi
out=$(curl --silent "${url}" -X POST -d "user_name=$user" -d "user_pass=$password" -d "action=grant")
exit=$(echo "$out" | jq .is_error)
if [ "$exit" != "0" ]; then
  echo "$out" | jq
  exit 1
fi
exit 0
