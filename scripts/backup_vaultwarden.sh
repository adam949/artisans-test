#!/bin/bash -e

# This script will copy the files and database from our VaultWarden isntance
# to the local computer in a directory called ./backups and the subdirectory
# will be the SSH username (to ensure production backups are never overwritten
# by test backups).
# 
# Usage: ./backup_vaultwarden.sh SSH_USERNAME SSH_PASSWORD DB_NAME
#
# This script expects to be next to the bypass_2fa.sh script, as it will use
# that to get access to the server.

! which rsync >/dev/null && printf "Please install rsync.\n" && exit 1

username="$1"
password="$2"
db_name="$3"
db_pass="$4"

if [ -z "$username" -o -z "$password" -o -z "$db_name" -o -z "$db_pass" ]; then
	echo ""
	echo "Usage: $0 SSH_USERNAME SSH_PASSWORD DB_NAME DB_PASSWORD"
	echo ""
	exit 1
fi

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
DATE=`date '+%Y-%m-%d'`

$SCRIPT_DIR/bypass_2fa.sh "$username" "$password"
mkdir -p backups/$username/files
rsync -av $username@shell.mayfirst.org:files/vaultwarden backups/$username/files
echo "export PGPASSWORD=\"$db_pass\"" | ssh $username@shell.mayfirst.org 'umask 077; cat > files/db_pass'
ssh $username@shell.mayfirst.org ". files/db_pass; pg_dump -h psql002.mayfirst.cx -U $db_name $db_name" > backups/$username/$DATE-postgresql.dump
ssh $username@shell.mayfirst.org ". files/db_pass; pg_dump -h psql002.mayfirst.cx -U $db_name --data-only $db_name" > backups/$username/$DATE-postgresql-dataonly.dump
