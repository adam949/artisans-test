#!/bin/bash -e

# This script will restore Vaultwarden data from a backup, both files and the
# database. It assumes there will be a ./backup/$ssh_username/files directory
# which is to be restored to the server. It also takes the filename of the
# database dump file to restore.
# 
# Usage: ./backup_vaultwarden.sh SSH_USERNAME SSH_PASSWORD DB_NAME
#
# This script expects to be next to the bypass_2fa.sh script, as it will use
# that to get access to the server.

! which rsync >/dev/null && printf "Please install rsync.\n" && exit 1

username="$1"
password="$2"
db_name="$3"
db_pass="$4"
db_file="$5"

if [ -z "$username" -o -z "$password" -o -z "$db_name" -o -z "$db_pass" -o -z "$db_file" ]; then
	echo ""
	echo "Usage: $0 SSH_USERNAME SSH_PASSWORD DB_NAME DB_PASSWORD DB_BACKUP_FILE"
	echo ""
	exit 1
fi

if [ ! -f "$db_file" ]; then
	echo ""
	echo "Error: $db_file not found"
	echo ""
	exit 1
fi

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

$SCRIPT_DIR/bypass_2fa.sh "$username" "$password"
rsync -av backups/$username/files/ $username@shell.mayfirst.org:files/
echo "export PGPASSWORD=\"$db_pass\"" | ssh $username@shell.mayfirst.org 'umask 077; cat > files/db_pass'
cat "$db_file" | ssh $username@shell.mayfirst.org ". files/db_pass; psql -h psql002.mayfirst.cx -U $db_name $db_name -X $db_name"
