# Artisans Cooperative

This repository is primarially being used for the [issue tracker](https://gitlab.com/artisans-coop/artisans-coop/-/boards) and [wiki](https://gitlab.com/artisans-coop/artisans-coop/-/wikis/home). This is intended to be mostly an internal resource, but we make it available to the general public so people have the option of checking to see if whatever they're running into is a known issue, comment on issues if they think they can help get them resolved, and so forth.

## Getting access
Any GitLab.com user can comment on our tickets, whether or not they are a member of this repository. To get access to create tickets, please contact your team lead to be added.

## Support
As a coop, different teams handle different types of request for support. Here's a quick reference to try to get you pointed in the right direction.

- Problems with an order: [Contact us](https://artisans.coop/pages/contact) page
- Issue with artisans.coop website: [website feedback](https://artisans.coop/pages/website-feedback) form
- Questions about becoming a coop member: [membership page](https://artisans.coop/pages/membership)
- Help with using this issue tracker: contact your team lead
- Interested in volunteering to be on a team: email [hi@artisans.coop](mailto:hi@artisans.coop)
- Other questions: email [hi@artisans.coop](mailto:hi@artisans.coop), or join our [Discord channel](https://blog.artisans.coop/blog/introduction-to-our-discord/)
- Anything else: email [hi@artisans.coop](mailto:hi@artisans.coop)
